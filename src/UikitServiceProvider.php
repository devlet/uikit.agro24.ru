<?php

namespace Agro24\Uikit;

use Illuminate\Support\ServiceProvider;

class UikitServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/resources/assets/' => resource_path('/assets/vendor/uikit/'),
            __DIR__ . '/resources/views/' => resource_path('/views/vendor/uikit/'),
        ]);
        $this->publishes([
            __DIR__ . '/resources/assets/js/' => public_path('/vendor/uikit/js/'),
            __DIR__ . '/resources/assets/css/' => public_path('/vendor/uikit/css/'),
        ], 'js_css');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('uikit', function ($app) {
            return new UikitBuilder($app['url'], $app['view']);
        });

        $this->app->alias('uikit', 'Agro24\Uikit\UikitBuilder');
    }

    public function provides()
    {
        return ['uikit', 'Agro24\Uikit\UikitBuilder'];
    }

}