<?php

namespace Agro24\Uikit;


use Illuminate\Support\Facades\Facade;

class UikitFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'uikit';
    }
}