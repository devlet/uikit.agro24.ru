<?php

namespace Agro24\Uikit;

use Illuminate\Support\ServiceProvider;

class RouteUikitServiceProvider extends ServiceProvider
{
    
    public function boot()
    {
        require __DIR__ . '/Http/routes.php';
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'Uikit');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        
    }
}