<?php

namespace Agro24\Uikit;


use BadMethodCallException;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use  Validator;
use Illuminate\Support\Traits\Macroable;

class UikitBuilder
{
    use Macroable, Componentable {
        Macroable::__call as macroCall;
        Componentable::__call as componentCall;
    }

    protected $url;

    /**
     * The View Factory instance.
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $view;

    /**
     * Create a new HTML builder instance.
     *
     * @param \Illuminate\Contracts\Routing\UrlGenerator $url
     * @param \Illuminate\Contracts\View\Factory $view
     */
    public function __construct(UrlGenerator $url = null, Factory $view)
    {
        $this->url = $url;
        $this->view = $view;
    }

    public function input($type, $param, $rules = [], $typeOfElement = null)
    {
        $param = $this->oldValue($param);
        $rand = str_random(5);
        $options = Helpers::parseString($param);
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }

        $options = array_merge(compact('type', 'rand'), $options);
        
        if (null !== $typeOfElement) {
            $options['typeOfElement'] = $typeOfElement;
        }
        switch ($type) {
            case 'radio':
            case 'checkbox':
                return view('Uikit::checkbox', $options);
            case 'live':
                return view('Uikit::live', $options);
            default:
                return view('Uikit::input', $options);
        }
    }

    public function text($param)
    {
        $rules = [
            'name' => 'required',
            'placeholder' => 'required',
            'label' => 'required'
        ];

        return $this->input('text', $param, $rules);
    }
    
    public function live($param)
    {
        $rules = [
            'name' => 'required',
            'ajax_url' => 'required'
        ];
        
        return $this->input('live', $param, $rules);
    }

    public function password($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('password', $param, $rules);
    }

    public function email($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('email', $param, $rules);
    }

    public function date($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('date', $param, $rules);
    }

    public function number($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('number', $param, $rules);
    }

    public function checkbox($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('checkbox', $param, $rules);
    }

    public function checkboxSlider($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('checkbox', $param, $rules, 'slider');
    }

    public function checkboxToggle($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('checkbox', $param, $rules, 'toggle');
    }

    public function radio($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('radio', $param, $rules, 'radio');
    }

    public function radioSlider($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('radio', $param, $rules, 'slider');
    }
    
    public function radioToggle($param)
    {
        $rules = [
            'name' => 'required',
            'label' => 'required'
        ];

        return $this->input('radio', $param, $rules, 'toggle');
    }
    
    public function searchProduct($param, $rules = [])
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        return view('Uikit::search-product', $options);
    }

    public function searchProductMulti($param, $rules = [])
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        $options['name'] = $options['name'] . '[]';
        return view('Uikit::search-product-multi2', $options);
    }

    public function searchProductMultiVue($param, $rules = [])
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        return view('Uikit::search-product-multi2-v', $options);
    }

    public function searchCity($param, $rules = [])
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        return view('Uikit::search-city', $options);
    }

    public function searchCityOne($param, $rules = [], $defaultCity = null)
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        if (empty($rules['name'])) {
            $rules['name'] = 'required';
        }
        if (empty($rules['ajax'])) {
            $rules['ajax_url'] = 'required';
        }
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        if (!is_null($defaultCity)) {
            $options['defaultCity'] = [
                'value' => $defaultCity['id'] . ',' . $defaultCity['type'],
                'name' => $defaultCity['name'],
            ];
        }
        return view('Uikit::search-city-one', $options);
    }


    public function button($param = '')
    {
        $options = Helpers::parseString($param);
        $validator = Validator::make($options, [
            'text' => 'required'
        ]);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        return view('Uikit::button', $options);
    }

    public function model($param)
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        if (empty($rules['name'])) {
            $rules['name'] = 'required';
        }
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        $options['items'] = $options['model']::all();

        return view('Uikit::model', $options);
    }

    public function lineAjaxModel($param)
    {
        $options = Helpers::parseString($param);
        $options['rand'] = str_random(5);
        if (empty($rules['name'])) {
            $rules['name'] = 'required';
        }
        $validator = Validator::make($options, $rules);
        if ($validator->fails()) {
            return 'не пройдена валидация'; // TODO Сделать вьюху для валидации
        }
        return view('Uikit::lineAjaxModel', $options);
    }

    /**
     * Dynamically handle calls to the class.
     *
     * @param  string $method
     * @param  array $parameters
     * @return \Illuminate\Contracts\View\View|mixed
     * @throws BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        try {
            return $this->componentCall($method, $parameters);
        } catch (BadMethodCallException $e) {
            //
        }

        try {
            return $this->macroCall($method, $parameters);
        } catch (BadMethodCallException $e) {
            //
        }

        return "Method {$method} does not exist.";
    }

    /**
     * Transform the string to an Html serializable object
     *
     * @param $html
     * @return \Illuminate\Support\HtmlString
     */
    protected function toHtmlString($html)
    {
        return new HtmlString($html);
    }

    /**
     * @param $param
     * @return string
     */
    protected function oldValue($param)
    {
        //TODO Реализовать заполнение старыми данными чекбоксы и радио
        if (!preg_match('/-c(\W|$)/', $param) && false === strpos('-v ', $param)) {
            $param = $param . ' -v ' . old('value');
            return $param;
        }
        return $param;
    }
    
}