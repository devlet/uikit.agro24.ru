<div class="uikit @if(!empty($class)){{ $class }}@endif">
    <div class="field">
        @if(isset($label))<label>{{ $label }}</label>@endif
        <input
            @if(isset($rand))id="{{$rand}}" @endif
            @if(isset($value))value="{{ $value }}" @endif
            @if(isset($name))name="{{ $name }}" @endif
            @if(isset($placeholder))placeholder="{{ $placeholder }}" @endif
            @if(isset($type))type="{{ $type }}"@endif
        >
    </div>
</div>