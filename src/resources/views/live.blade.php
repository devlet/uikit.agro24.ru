<div class="live ui input @if(!empty($wrapper_class)){{ $wrapper_class }}@endif" v-uikit="live">
    <div class="field">
        @if(!empty($label))<label>{{ $label }}</label>@endif
        <input
            @if(!empty($value))value="{{ $value }}" @endif
            @if(!empty($name))name="<?php echo '{{' . $name . '}}'?>" @endif
            @if(!empty($placeholder))placeholder="{{ $placeholder }}" @endif
            @if(!empty($type))type="{{ $type }}"@endif
            @if(!empty($ajax_url))data-ajax="<?php echo '{{' . $ajax_url . '}}'?>" @endif
        >
    </div>
</div>

@push('uikit-scripts')
<script id="script-live">
    (function(element) {
        if(!element) return false;
        var elem = $(element);
        var input = elem.find('input');
        input.on('change', function(e) {
            if (e.target.value === '') return false;
            $.get(input.attr('data-ajax'), {
                name: input.attr('name'),
                value: input.val()
            }, function (data) {
                console.log(data.success);
            }, 'json');
        })
    })(this.el)
</script>
@endpush