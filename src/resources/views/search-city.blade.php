<div class="multi-search v{{ $rand }}" v-uikit="city">
    <div class="field label">
        <label>@if(!empty($label)){{ $label }} @else Продукт @endif </label>
    </div>
    <div class="field">
        <div class="ui search selection dropdown v{{ $rand }}">
            <input name="<?php echo '{{' . $name . '}}'?>" type="hidden">
            <i class="dropdown icon"></i>
            <input type="text" class="search" tabindex="0">
            <div class="default text">@if(!empty($placeholder)){{ $placeholder }}@else Поиск @endif</div>
            <div class="menu" tabindex="-1"></div>
        </div>
    </div>
</div>

@push('uikit-scripts')
<script type="template/script" id="script-city">
    (function (element) {
        if (!element) return;
        var elem = $(element);
        var innerElem = elem.find('.dropdown.v{{ $rand }}');
        innerElem.dropdown({
            apiSettings: {
                url: 'http://app.stage.agro24.ru/api/sys/location?search={query}',
                //url: '/cityajax?search={query}',
                onResponse: function(json) {
                    if(!json.success) return false;
                    items = [];
                    total_items = json.total;

                    if (total_items == 0) return {
                        "success": true,
                        "results": items
                    };
                    json.items.forEach(function(item) {
                        var html = '<div class="city">' + item.name + '</div>';
                        if (item.desc) {
                            html += '<div class="desc">' + item.desc + '</div>'
                        }
                        items.push({value: item.id, name: html});
                    });
                    return {
                        "success": true,
                        "results": items
                    }
                }
            },
            minCharacters: 1,
            //saveRemoteData: false,
        });
    })(this.el);
</script>
@endpush