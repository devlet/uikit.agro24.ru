<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <link rel="stylesheet" href="{{ asset('vendor/uikit/css/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="ui container">
    <h2>Простые эелементы</h2>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Текстовое поле</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::text('-n name -p Введите что нибудь -l Text') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::text('-n name -p Введите что нибудь -l Text') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -p
                                    </div>
                                </h4>
                            </td>
                            <td>
                                placeholder
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -v
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут value
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Поле для ввода пароля</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::password('-n password -l Password') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::password('-n password -l Password') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -v
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут value
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Поле для ввода email</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::email('-n email -l Email') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::email('-n email -l Email') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -v
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут value
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -p
                                    </div>
                                </h4>
                            </td>
                            <td>
                                placeholder
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Поле для ввода даты</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::date('-n date -l Date') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::date('-n date -l Date') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -v
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут value
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Цифровое поле</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::number('-n number -l Number') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::number('-n number -l Number') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -v
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут value
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -p
                                    </div>
                                </h4>
                            </td>
                            <td>
                                placeholder
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Простой чекбокс</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::checkbox('-n check -l Checkbox -c') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::checkbox('-n check -l Checkbox -c') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Чекбокс слайдер</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::checkboxSlider('-n checkSlider -l Checkbox_slider') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::checkboxSlider('-n checkSlider -l Checkbox_slider') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Чекбокс переключатель</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::checkboxToggle('-n checkToggle -l Checkbox_toggle -c') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::checkboxToggle('-n checkToggle -l Checkbox_toggle -c') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Радио</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::radio('-n radio -l first -c') !!}
                {!! Uikit::radio('-n radio -l second') !!}
                {!! Uikit::radio('-n radio -l third') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::radio('-n radio -l first -c') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radio('-n radio -l second') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radio('-n radio -l third') !!}</span><br>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Радио слайдеры</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::radioSlider('-n radio2 -l first') !!}
                {!! Uikit::radioSlider('-n radio2 -l second -c') !!}
                {!! Uikit::radioSlider('-n radio2 -l third') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::radioSlider('-n radio2 -l first') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radioSlider('-n radio2 -l first -c') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radioSlider('-n radio2 -l first') !!}</span><br>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Радио переключатели</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::radioToggle('-n radio3 -l first') !!}
                {!! Uikit::radioToggle('-n radio3 -l second -c') !!}
                {!! Uikit::radioToggle('-n radio3 -l third') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::radioToggle('-n radio3 -l first') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radioToggle('-n radio3 -l first -c') !!}</span><br>
            <span style="font-size: 1.4em;">@{!! Uikit::radioToggle('-n radio3 -l first') !!}</span><br>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -c
                                    </div>
                                </h4>
                            </td>
                            <td>
                                отметить checked
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Кнопка</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::button('-tx Зарегистрироваться -class disable -w_class test') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::button('-tx Зарегистрироваться -class disable -w_class test') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -tx
                                    </div>
                                </h4>
                            </td>
                            <td>
                                Тексе на кнопке
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -w_class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс обертки
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс кнопки
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <h2>Сложные одиночные</h2>
    <div class="ui raised  segments">
        <div class="ui segment">
            <h3>Поиск одиночноего продукта</h3>
        </div>
        <div class="ui green segment">
            <div class="ui form">
                {!! Uikit::searchProduct('-n search -l Найти продукт -p Название продукта') !!}
            </div>
        </div>
        <div class="ui red segment">
            <span style="font-size: 1.4em;">@{!! Uikit::searchProduct('-n search -l Найти продукт -p Название продукта') !!}</span>
        </div>
        <div class="ui blue segment">
            <div class="ui grid two column">
                <div class="column">
                    <h3>Обязательные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -n
                                    </div>
                                </h4>
                            </td>
                            <td>
                                атрибут name
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="column">
                    <h3>Доступные параметры</h3>
                    <table class="ui very basic collapsing celled table">
                        <thead>
                        <tr>
                            <th>Параметр</th>
                            <th>Описание</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -l
                                    </div>
                                </h4>
                            </td>
                            <td>
                                label
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -p
                                    </div>
                                </h4>
                            </td>
                            <td>
                                placeholder
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="ui header">
                                    <div class="content">
                                        -class
                                    </div>
                                </h4>
                            </td>
                            <td>
                                класс
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="ui segments">
        <div class="ui segment">
            <div class="ui form">
                {!! Uikit::searchProductMulti('-n searchMulti2 -l Выбрать много продуктов -p Название продукта') !!}
            </div>
        </div>
        <div class="ui red segment">
            Для простой вставки в код <br>
            @{!! Uikit::searchProductMulti('-n searchMulti2 -l Выбрать много продуктов -p Название продукта') !!}<br>
            <br>
            При использовании в Vue <br>
            @{!! Uikit::searchProductMultiVue('-n searchMulti -l Выбрать много продуктов -p Название продукта') !!}<br>
            <strong>Необходимо использовать в Vue</strong><br>
            Добавить в скрипты:
            <pre>
    Vue.directive('uikit', {
            bind: function() {
            var script = document.getElementById('script-' + this.expression).innerHTML;
            eval(script);
        }
    });
            </pre>
            Добавить в главном лойатe: &commat;stack('uikit-scripts')
        </div>
        <div class="ui blue segment">
            Обязательные параметры
            <pre>
    -n name
    -l label
    -p placeholder
            </pre>
        </div>
    </div>
    <div class="ui segments">
        <div class="ui segment">
            <div class="ui form">
                {!! Uikit::live('-n item.name -ajax item.ajax') !!}
            </div>
        </div>
        <div class="ui red segment">
            @{!! Uikit::live('-n item.name -ajax item.ajax') !!}<br>
            <strong>Необходимо использовать в Vue</strong><br>
            Добавить в скрипты:
            <pre>
    Vue.directive('uikit', {
            bind: function() {
            var script = document.getElementById('script-' + this.expression).innerHTML;
            eval(script);
        }
    });
            </pre>
            Добавить в главном лойатe: &commat;stack('uikit-scripts')
        </div>
        <div class="ui blue segment">
            Обязательные параметры
            <pre>
    -n name
    -ajax url for ajax request
            </pre>
        </div>
    </div>
    <div class="ui segments">
        <div class="ui segment">
            <div class="ui form">
                {!! Uikit::searchCity('-n item.name -l Выбрать город -p Название города') !!}
            </div>
        </div>
        <div class="ui red segment">
            @{!! Uikit::searchCity('-n item.name -l Выбрать город -p Название города') !!}<br>
            <strong>Необходимо использовать в Vue</strong><br>
            Добавить в скрипты:
            <pre>
    Vue.directive('uikit', {
            bind: function() {
            var script = document.getElementById('script-' + this.expression).innerHTML;
            eval(script);
        }
    });
            </pre>
            Добавить в главном лойатe: &commat;stack('uikit-scripts')
        </div>
        <div class="ui blue segment">
            Обязательные параметры
            <pre>
    -n name
            </pre>
        </div>
    </div>
    <div class="ui segments">
        <div class="ui segment">
            <div class="ui form">
                {!! Uikit::searchCityOne('-n item.name -l Выбрать город -p Название города -ajax http://app.stage.agro24.ru/api/sys/location') !!}
            </div>
        </div>
        <div class="ui red segment">
            @{!! Uikit::searchCityOne('-n searchCity -l Выбрать город -p Название города -ajax http://app.stage.agro24.ru/api/sys/location', [], $defaultCity) !!}
            <br>
            Добавить в главном лойатe: &commat;stack('uikit-scripts')<br>

            Город по умолчанию <br>
            <pre>
            $defaultCity = [
                'id' => id города,
                'name' => Название города,
                'type' => тип города
            ]
            </pre>
        </div>
        <div class="ui blue segment">
            Обязательные параметры
            <pre>
    -n name
    -ajax api url for ajax request
            </pre>
        </div>
    </div>

</div>
<script src="{{ asset('vendor/uikit/js/jquery-3.1.0.min.js') }}"></script>
<script src="{{ asset('vendor/uikit/js/semantic.min.js') }}"></script>
@stack('uikit-scripts')
</body>
</html>