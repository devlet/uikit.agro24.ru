<div class="ui @if(isset($max) && $max > 0) multiple @endif dropdown" id="{{$rand}}">
    <input type="hidden" name="{{ $name }}">
    @if(!empty($icon))<i class="{{$icon}} icon"></i>@endif
    <div class="default text">{{ $placeholder }}</div>
    <div class="menu">
        <div class="ui icon search input">
            <i class="search icon"></i>
            <input type="text" placeholder="Поиск">
        </div>
        <div class="divider"></div>
        <div class="scrolling menu">
        </div>
    </div>
</div>

@push('uikit-scripts')
<script>
    (function(elem) {
        if (!elem) return;
        var timeout;
        elem.dropdown({
            maxSelections: {{$max ?? 'false'}},
            searchFullText: true,
            fullTextSearch: true,
            useLabels: {{ isset($no_label) ? 'false': 'true' }},
            message:  {
                count         : 'Выбрано: {count}',
            }
        });

        elem.find('.input input').on('keyup', function() {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout((function() {
                loadItems($(this).val())
            }).bind(this), 500);
        });

        var searchItems = {};

        loadItems();

        function loadItems(search, ids) {
            if (typeof search == "undefined") search = "";
            if (typeof searchItems[search] == "undefined")
            {
                $.ajax({
                    url: "{{$ajax_url}}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    data: {
                        "per_page": 5,
                        "name": search,
                        "id": ids,
                    }
                }).then(function (json) {
                    searchItems[search] = json.items.data;
                    pasteResult(json.items.data);
                });
            }
            else
            {
                pasteResult(searchItems[search]);
            }
        }

        function pasteResult(result) {
            var menu = elem.find('.scrolling.menu');
            menu.html('');
            result.forEach(function(item) {
                var newElem = $('<div>');
                newElem.addClass('item');
                newElem.text(item.name);
                newElem.attr('data-value', item.id)
                menu.append(newElem);
            });
        }

    })($('#{{ $rand }}'))
</script>
@endpush
