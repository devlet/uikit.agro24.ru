<div class="button-container @if(!empty($wrapper_class)){{ $wrapper_class }}@endif">
    <button type="@if(!empty($type)){{ $type }}@else button @endif" class="kit labeled icon button @if(!empty($class)){{ $class }}@endif">
        <i class="right Spinner animation icon"></i>
        @if(!empty($text)){{ $text }}@endif
    </button>
</div>