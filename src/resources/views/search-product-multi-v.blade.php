<div class="multi-search v{{ $rand }}" v-uikit="product2">
    <div class="field label">
        <label>@if(!empty($label)){{ $label }} @else Продукт @endif </label>
    </div>
    <div class="field">
        <div class="ui search selection dropdown v{{ $rand }}">
            <input name="{{ $name }}" type="hidden">
            <i class="dropdown icon"></i>
            <input type="text" class="search" tabindex="0">
            <div class="default text">@if(!empty($placeholder)){{ $placeholder }}@else Поиск @endif</div>
            <div class="menu" tabindex="-1"></div>
        </div>
        <button type="button" class="addButton">+</button>
        <ul class="searchList v{{ $rand }}"></ul>
    </div>
</div>

@push('uikit-scripts')
<script  type="template/script" id="script-product2">
    (function (element) {
        var isLastField = true;
        var innerElem = element.querySelector('.' + 'v{{ $rand }}');

        (function drop(elem) {
            var isButtonAdded = false;
            var elem = $(elem);
            var newField = elem.clone();
            elem.dropdown({
                apiSettings: {
                    url: '/api/sys/searchProduct40?name={query}',
                    onResponse: function (json) {
                        items = [];
                        total_items = json.total;

                        //console.log("total " + total_items);

                        if (total_items == 0) return {
                            "success": true,
                            "results": items
                        };

                        json.items.forEach(function (item) {

                            //console.log(item);

                            var href_all = "#" + item.id;

                            //Укорачиваем строку и ставим ...
                            var P04 = json.P04 || 30;

                            if (item.name.length > P04) item.shortName = item.name.substring(0, P04) + "...";
                            else  item.shortName = item.name;

                            //Добавляем параметры
                            if (item.desc != null)
                                item.shortName = item.shortName + "<br><small class='measure'>" + item.desc + '</small>';

                            //Шаблон
                            if (item.type_tovar_product != 1) //Если продукт
                            {
                                temp = '<div class="ui__select_product" data-name="' + item.name + '">' + item.shortName + '</div>';
                            }
                            else {
                                temp = '<div class="ui__select_goods" data-name="' + item.name + '">' + item.shortName + '</div><i class="ui__label_goods"></i>';
                            }

                            items.push({value: item.id, rus: item.name, name: temp, "product": 1});
                        });
                        if (total_items > 0)
                            var temp = '<div class="ui__select_all">Смотреть еще (всего ' + total_items + ' позиций)</div>';
                        items.push({value: -1, name: temp});

                        return {
                            "success": true,
                            "results": items
                        };

                    }
                },
                minCharacters: 3,
                saveRemoteData: false,
                onChange: function (value, text, $choice) {
                    var wrapper = $(element);
                    var field = $('<div class="field"></div>');
                    if (value == -1) {
                        return false;
                    }
                    $(this).find('.text').html($(text).attr('data-name'))
                    var buttonMore = $('<button class="more">More</button>');
                    buttonMore.on('click', function (e) {
                        e.preventDefault();
                        buttonMore.remove();
                        wrapper.append(field.append(newField));
                        isLastField = false;
                        drop(newField);
                    });
                    var buttonRemove = $('<button class="remove">&times;</button>');
                    buttonRemove.on('click', function (e) {
                        e.preventDefault();
                        if (!isLastField) {
                            elem.parent().remove();
                            if ($(element).find('.field:not(.label)').length == 1) {
                                isLastField = true;
                            };
                        } else {
                            elem.dropdown('restore defaults');
                        }

                    })
                    if (!isButtonAdded) {
                        wrapper.append(buttonMore);
                        elem.parent().append(buttonRemove);
                    }
                    isButtonAdded = true;
                }
            });
        })(innerElem);
    })(this.el);
</script>
@endpush