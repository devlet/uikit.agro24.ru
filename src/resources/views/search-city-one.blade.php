<div class="uikit search-city @if(!empty($class)) {{ $class }}  @endif" id="{{ $rand }}">
    <div class="field label">
        <label>@if(!empty($label)){{ $label }} @else Продукт @endif </label>
    </div>
    <div class="field">
        <div class="ui search selection dropdown v{{ $rand }}" data-ajax="{{ $ajax_url }}">
            <input name="{{ $name . '.id' }}" type="hidden" value="@if(!empty($defaultCity)){{ $defaultCity['value'] }}@endif">
            <input name="{{ $name. '.type' }}" type="hidden">
            <i class="dropdown icon"></i>
            <input type="text" class="search" tabindex="0">
            <div class="default text">@if(!empty($placeholder)){{ $placeholder }}@else Поиск @endif</div>
            <div class="menu" tabindex="-1">
                @if(!empty($defaultCity))
                    <div class="item" data-value="{{ $defaultCity['value'] }}" data-text="{{ $defaultCity['name'] }}">
                        <div class="city">{{ $defaultCity['name'] }}</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('uikit-scripts')
<script>
    (function (elem) {
        if (!elem) return;
        var innerElem = elem.find('.dropdown.v{{ $rand }}');
        innerElem.dropdown({
            apiSettings: {
                url: innerElem.attr('data-ajax') + '?search={query}',
                onResponse: function (json) {
                    if (!json.success) return false;
                    var items = [];
                    var total_items = json.total;

                    if (total_items == 0) return {
                        "success": true,
                        "results": items
                    };
                    json.items.forEach(function (item) {
                        var html = '<div class="city">' + item.name + '</div>';
                        if (item.desc) {
                            html += '<small class="desc">' + item.desc + '</small>'
                        }
                        items.push({value: [item.id, item.type], text: item.name, name: html});
                    });
                    return {
                        "success": true,
                        "results": items
                    }
                },
                throttle: 2
            },
            minCharacters: 2,
            saveRemoteData: false,
            onChange: function (value, text, $choice) {
                var array = value.split(',');
                $(this).find('input[type=hidden]:first-child').val(array[0]);
                $(this).find('input[type=hidden]:nth-child(2)').val(array[1]);
            },
        });
        var value = innerElem.find('input[type=hidden]:first-child').val().split(',');
        innerElem.find('input[type=hidden]:first-child').val(value[0]);
        innerElem.find('input[type=hidden]:nth-child(2)').val(value[1]);
    })($('#{{ $rand }}'));
</script>
@endpush