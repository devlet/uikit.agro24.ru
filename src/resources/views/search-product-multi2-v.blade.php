<div class="multi-search v{{ $rand }}" v-uikit="product">
    <div class="field label">
        <label>@if(!empty($label)){{ $label }} @else Продукт @endif </label>
    </div>
    <div class="field">
        <div class="ui search selection dropdown v{{ $rand }}">
            <input name="<?php echo '{{' . $name . '}}[]'?>" type="hidden">
            <i class="dropdown icon"></i>
            <input type="text" class="search" tabindex="0">
            <div class="default text">@if(!empty($placeholder)){{ $placeholder }}@else Поиск @endif</div>
            <div class="menu" tabindex="-1"></div>
        </div>
        <button type="button" class="addButton">+</button>
        <ul class="searchList v{{ $rand }}"></ul>
    </div>
</div>

@push('uikit-scripts')
<script  id="script-product">
        (function (element) {
            if (!element) return;
            var elem = $(element);
            var button = elem.find('.addButton');
            var innerElem = elem.find('.dropdown.v{{ $rand }}');

            function activateAdd() {
                var newItem = $('<li></li>');
                var newText = innerElem.find('.text').text();
                newItem.text(newText);

                var removeButton = $('<button>&times;</button>');
                removeButton.on('click', function (e) {
                    e.preventDefault();
                    $(this).parent().remove();
                });
                newItem.append(removeButton);
                var parentInput = innerElem.find('input[type=hidden]');
                var input = $('<input type="hidden">');
                input.attr('name', parentInput.attr('name'));
                input.val(parentInput.val());
                newItem.append(input);
                var ul = elem.find('ul.searchList.v{{ $rand }}');
                ul.append(newItem);
                innerElem.dropdown('restore defaults');
                button.off('click', activateAdd);
            }

            innerElem.dropdown({
                apiSettings: {
                    url: '/api/sys/searchProduct40?name={query}',
                    onResponse: function (json) {
                        items = [];
                        total_items = json.total;

                        if (total_items == 0) return {
                            "success": true,
                            "results": items
                        };

                        json.items.forEach(function (item) {
                            var href_all = "#" + item.id;
                            var P04 = json.P04 || 30;

                            if (item.name.length > P04) item.shortName = item.name.substring(0, P04) + "...";
                            else  item.shortName = item.name;

                            if (item.desc != null)
                                item.shortName = item.shortName + "<br><small class='measure'>" + item.desc + '</small>';

                            if (item.type_tovar_product != 1)
                            {
                                temp = '<div class="ui__select_product" data-name="' + item.name + '">' + item.shortName + '</div>';
                            }
                            else {
                                temp = '<div class="ui__select_goods" data-name="' + item.name + '">' + item.shortName + '</div><i class="ui__label_goods"></i>';
                            }

                            items.push({value: item.id, rus: item.name, name: temp, "product": 1});
                        });
                        if (total_items > 0)
                            var temp = '<div class="ui__select_all">Смотреть еще (всего ' + total_items + ' позиций)</div>';
                        items.push({value: -1, name: temp});

                        return {
                            "success": true,
                            "results": items
                        };
                    }
                },
                minCharacters: 3,
                saveRemoteData: false,
                onChange: function (value, text, $choice) {
                    if (value == -1) {
                        return false;
                    }
                    $(this).find('.text').html($(text).attr('data-name'))
                    button.on('click', activateAdd);
                }
            });
        })(this.el);
</script>
@endpush