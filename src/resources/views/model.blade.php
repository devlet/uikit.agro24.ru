<div class="field model @if(!empty($class)) {{ $class }}@endif">
    @if(!empty($label))<label>{{ $label }}</label>@endif
    <div id="{{$rand}}" class="ui fluid search selection dropdown @if(isset($max) && $max > 0) multiple @endif">
        <input type="hidden" name="{{ $name }}">
        <i class="dropdown icon"></i>
        <div class="default text">@if(!empty($placeholder)){{ $placeholder }}@else Выбрать @endif</div>
        <div class="menu">
            @foreach($items as $item)
                <div class="item" data-value="{{$item->id}}">{{$item->name}}</div>
            @endforeach
        </div>
    </div>
</div>

@push('uikit-scripts')
<script>
    (function(element) {
        var values =  '{{$value ?? ''}}';
        var noLabels = '{{ $nl ?? '' }}';
        noLabels == 0 ? noLabels = true : noLabels = false;
        $(element).dropdown({
            maxSelections: '{{$max ?? 5}}',
            searchFullText: true,
            fullTextSearch: true,
            useLabels: noLabels,
            message:  {
                count: 'Выбрано: {count}',
            }
        });
        if (values != '')
            values.split(",").forEach(function(item) {

                $(element).dropdown('set selected', item);

            });
    })('#{{ $rand }}');
</script>
@endpush