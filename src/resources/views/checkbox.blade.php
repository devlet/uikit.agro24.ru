<div class="field">
    <div class="ui @if(!empty($typeOfElement)){{ $typeOfElement }}@endif checkbox">
        <input
            @if(isset($type))type="{{ $type }}" @endif
            @if(isset($name))name="{{ $name }}" @endif
            @if(!empty($checked))checked @endif
            @if(isset($rand))id="{{ $rand }}"@endif
        >
        @if(isset($label))<label @if(isset($rand))for="{{ $rand }}"@endif>{{ $label }}</label>@endif
    </div>
</div>
