<?php

namespace Agro24\Uikit;


class Helpers
{
    public static function parseString($str, $attr = null) {

        $array = explode("-", $str);
        if ($attr == null)
            $attr = [
                'n' => 'name', // name
                'i' => 'items', // items
                'nl' => 'no_label', // no-label
                'v' => 'value', // value
                'p' => 'placeholder', // placeholder
                'max' => 'max', // max selection on values
                'many' => 'multi', // multiselect
                't' => 'table', // table
                'm' => 'model', // model
                'l' => 'label', // label
                'ajax' => 'ajax_url', // url for ajax
                'icon' => 'icon', // icon
                'class' => 'class', // class
                'w_class' => 'wrapper_class', //wrapper class
                'c' => 'checked', //checked
                'tx' => 'text' //text
            ];

        $keys = [];

        foreach($array as $str) {
            preg_match('/^\w+/', $str, $matches);
            if (count($matches) > 0) {
                switch ($matches[0]) {
                    case 'c':
                        $keys[$attr[$matches[0]]] = 'checked';
                        break;
                    case 'nl':
                        $keys[$attr[$matches[0]]] = '1';
                        break;
                    default:
                         $keys[$attr[$matches[0]]] = trim(str_replace($matches[0] . ' ', '', $str));
                };

            }
        }
        return $keys;
    }
}