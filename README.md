# Uikit

## Установка

1\. Добавить в файл composer.json проекта:

```
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:devlet/uikit.agro24.ru.git"
            или
            "url": "https://ВАШ_НИК@bitbucket.org/devlet/uikit.agro24.ru.git"
        }
],
"require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.2.*",
        "Agro24/Uikit": "dev-master"
},
```

2\. Выполнить команду

``` bash
$ composer update
```

3\. Добавить в файл config/app.php
3\.1 в секцию провайдеров

```php
Agro24\Uikit\UikitServiceProvider::class,
Agro24\Uikit\RouteUikitServiceProvider::class,
```
3\.2 в секцию фасадов

```php
'Uikit' => Agro24\Uikit\UikitFacade::class,
```

4\. Опубликовать ресурсы

```bash
$ php artisan vendor:publish
```

## Использование

http://ваш_проекс/uikit
Скопировать строку кода и вставить в blade шаблон. При наличии дополнительных инструкций в разделе элемента выполнить их.

## Обновление

1\. Выполнить команду

```bash
$ composer update
```

2\. При необходимости перепубликовать ресурсы

```bash
$ php artisan vendor:publish --force
```

